
//VALIDACIÓN MAYOR DE 18
$(document).ready(function () {

    FormValidation.Validator.mayorEdad = {
        validate: function (validator, $field, options) {
            var value = $field.val();


            var fechanacimiento = moment(value, "DD-MM-YYYY");

            if (!fechanacimiento.isValid())
                return false;

            var years = moment().diff(fechanacimiento, 'years');

            return years > 18;

        }
    };

    $('#formularioRegistro').formValidation({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            txtFecha: {
                validators: {
                    notEmpty: {
                        message: ' La fecha de nacimiento es requerida'
                    },
                    mayorEdad: {
                        message: ' No es mayor de edad'
                    }
                }
            }
        }
    });
});



//VALIDACION SOLO LETRAS

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function limpia() {
    var val = document.getElementById("miInput").value;
    var tam = val.length;
    for (i = 0; i < tam; i++) {
        if (!isNaN(val[i]))
            document.getElementById("miInput").value = '';
    }
}

//VALIDACION SOLO NÚMEROS
function validaNumericos(event) {
    if (event.charCode >= 48 && event.charCode <= 57) {
        return true;
    }
    return false;
}

//VALIDACION FORMLARIO
$("#form1").validate({
    rules: {
        "txtCorreo": {
            required: true,
            email: true
        },
        "txtRun": {
            required: true
        },
        "txtNombre": {
            required: true
        },
        "txtFecha": {
            required: true
        },
        "txtTelefono": {
            required: true
        }
    }, // --> Fin de reglas
    messages: {
        "txtCorreo": {
            required: ' Ingrese correo electrónico',
            email: ' Debe ingresar un correo válido'
        },
        "txtRun": {
            required: ' Ingrese su Run'
        },
        "txtNombre": {
            required: ' Ingrese su nombre completo'
        },
        "txtFecha": {
            required: ' Debe ingresar su fecha de nacimiento'
        },
        "txtTelefono": {
            required: ' Debe ingresar un teléfono'
        }
    } //-->Fin de mensajes

});

//REGIONES Y CIUDADES

var opciones = {
    region1: ["Iquique"],
    region2: ["Antofagasta"],
    region3: ["Atacama"],
    region4: ["Coquimbo", "La Serena"],
    region5: ["Valparaíso", "San Antonio", "Los Andes"],
    region6: ["Rancagua"],
    region7: ["Talca"],
    region8: ["Concepción"],
    region9: ["Temuco"],
    region10: ["Puerto Montt"],
    region11: ["Coyhaique"],
    region12: ["Punta Arenas"],
    region13: ["Santiago"],
    region14: ["Valdivia"],
    region15: ["Arica"],
    region16: ["Chillán"]
};

// ahora vamos a definir nuestra funcion anonima
// la cual hará la gestión, es decir, captura el
// item seleccionado del primer combo y bla...

$(function () {
    var cargarCmbDos = function () {
        // capturamos el item seleccionado del primer combo
        var seleccion = $('#id_cmbPrimero').val();
        // ahora limpiamos el segundo combo antes
        // de cargar la lista
        $('#id_cmbSegundo').empty();

        opciones[seleccion].forEach(function (element, index) {
            $('#id_cmbSegundo').append('<option value="' + element + '">' + element + '</option');
        });
        $('#id_cmbPrimero').change(cargarCmbDos);
    };
    cargarCmbDos();
});






